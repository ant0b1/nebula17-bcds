#!/usr/bin/python
#indica l'esecuzione di python

import os # libreria per utilizzare funzioni del sistema operativo
import pickle #The pickle module implements binary protocols for serializing and de-serializing a Python object structure.
import time #This module provides various time-related functions. For related functionality, see also the datetime and calendar modules.
import socket #This module provides access to the BSD socket interface. It is available on all modern Unix systems, Windows, MacOS, and probably additional platforms.
import signal #This module provides mechanisms to use signal handlers in Python.


signal.signal(signal.SIGCHLD, signal.SIG_IGN)
"""The signal.signal() function allows defining custom handlers to be executed when a signal is received. 
signal.SIGCHLD Child process stopped or terminated.
signal.SIG_IGN This is another standard signal handler, which will simply ignore the given signal.
"""

def server(skt): #definisce una funzione "server" con parametro "skt"
  line = skt.recv(1024)   
"""
Riceve i dati dal socket. Il valore restituito è una stringa che rappresenta i dati ricevuti. 
La quantità massima di dati da ricevere contemporaneamente è specificata da buff-size.
RETURN : Stringa rappresentativa dei dati ricevuti
"""
  obj = pickle.loads(line)
"""
Deserializza una stringa di byte. Guardando bene il costruttore "Unpickler" possiamo notare come
viene usato __reduce__. Questo può essere sovrascritto e possiamo costruire un oggetto con una __reduce__ speciale in modo da
venir deserializzato come noi preferiamo. In particolare possiamo indicare nel metodo __reduce__ un oggetto chiamabile e
una tupla da 2 a 6 elementi per l'oggetto chiamabile. Quindi è qui la vera e propria vulnerabilità. 
Possiamo inviare un oggetto chiamabile ed una tupla che verrà eseguita con l'oggetto chiamabile e far eseguire ciò che più ci piace come ad esempio:
 # os.system come oggetto ed una shell remota
 # os.system come oggetto ed un comando qualsiasi, preferibilmente stampato in un file con > /percorso/del/file.txt
 # magari anche file in python, oppure altro @da pensarci

"""

  for i in obj:
      clnt.send("why did you send me " + i + "?\n")
"""
Invia dati al socket. Il socket deve essere collegato ad un socket remoto. Le applicazioni 
sono responsabili di controllare che tutti i dati siano stati inviati; se sono stati trasmessi 
solo alcuni dei dati, l’applicazione deve tentare la consegna dei dati rimanenti. 
RETURN: numero di byte inviati
"""
skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
"""
Crea un nuovo socket usando la family come indirizzo, il type di socket e il numero di 
protocol. 
RETURN: Oggetto socket i cui metodi implementano le varie chiamate di sistema socket.
"""
skt.bind(('0.0.0.0', 10007))
#Associa l'indirizzo al socket con la porta scelta
skt.listen(10)
#massimo 10 connessioni non accettate

while True:
  clnt, addr = skt.accept()
"""accetta la connessione e ritorna un nuovo socket "clnt" 
che possiamo usare per inviare e ricevere nuovi oggetti sulla connessione
addr è l'indirizzo legato al socket all'altro lato della connessione"""
  if(os.fork() == 0): #Effettua la fork di un processo figlio. RETURN: Restituisce 0 nel figlio e l’ID del processo figlio nel genitore
      clnt.send("Accepted connection from %s:%d" % (addr[0], addr[1]))
      server(clnt)
      exit(1)